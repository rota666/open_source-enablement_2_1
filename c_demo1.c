#include <stdio.h>
#include <math.h>


// 判断是否为三角形
void isTriangle(double a, double b, double c)
{
    if (a + b > c && a + c > b && b + c > a)
    {
        return 1;
    }
    return 0;
}

// 求三角形的面积
double areaOfTriangle(double a, double b, double c)
{

    double p = (a + b + c) / 2;
    return sqrt(p * (p - a) * (p - b) * (p - c));

}

int main(void)
{   
    double a, b, c;
    scanf("%lf %lf %lf", &a, &b, &c);
    if (isTriangle(a, b, c))
    {
        printf("s = %f\n", areaOfTriangle(a, b, c));
    }
    return 0;
}

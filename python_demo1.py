#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 计算2个列表所有元素的中位数
def findMedianSortedArrays(nums1, nums2):
    """
    :type nums1: List[int]
    :type nums2: List[int]
    :rtype: float
    """
    nums = nums1 + nums2
    nums.sort()
    n = len(nums)
    if n % 2 == 0：
        return (nums[n // 2 - 1] + nums[n // 2]) / 2
    else：
        return nums[n // 2]


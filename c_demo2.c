# include <stdio.h>

int main(void)
{
    int str1[2][10] = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}};
    int str2[2][10] = str1;
    int len = 0;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            printf("%d ", str2[i][j]);
        }
    }
}